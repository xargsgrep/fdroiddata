Categories:Internet,Security
License:MIT
Web Site:http://littlefox94.github.io/PGPAuth
Source Code:https://github.com/LittleFox94/PGPAuth
Issue Tracker:https://github.com/LittleFox94/PGPAuth/issues

Auto Name:PGPAuth
Summary:Send PGP-verified requests
Description:
This app sends GPG-verified requests over the internet to a given server.

Currently there are the actions open and close, but request-type will be configurable.
The request also includes a timestamp to verify the request is sent by the owner of the gpg-key and is not sent again by someone else.

It is used in the ChaosChemnitz hackerspace in Germany to open and close the door without the need of a "real" key.

You'll need [[org.thialfihar.android.apg]] to sign requests.
.

Repo Type:git
Repo:https://github.com/LittleFox94/PGPAuth.git

Build:1.2.1,7
    commit=v1.2.1

Build:1.3.1,8
    commit=v1.3.1

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3.1
Current Version Code:8

