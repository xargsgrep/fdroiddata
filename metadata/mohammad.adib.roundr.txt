Categories:System
License:Apache2
Web Site:http://forum.xda-developers.com/showthread.php?t=2234079
Source Code:https://github.com/MohammadAdib/RoundR
Issue Tracker:https://github.com/MohammadAdib/RoundR/issues

Auto Name:RoundR
Summary:Round the corners of the screen
Description:
RoundR takes advantage of the fact that regardless of the device's color, the
screen is surrounded with a pitch black border, the corners of which are
rarely ever touched. RoundR places four small overlays at the corners of the
display, to simulate a rounded screen.
.

Repo Type:git
Repo:https://github.com/MohammadAdib/Roundr.git

Build Version:1.7.2,10,406b35c7,subdir=RoundR,update=.;../StandOutRoundR,rm=RoundR.apk
Build Version:1.9.9,14,c5bb6b51,subdir=RoundR,update=.;../StandOutRoundR,rm=RoundR.apk,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/
Build Version:2.0.0.1,16,3121961041c3a0,subdir=RoundR,update=.;../StandOutRoundR,rm=RoundR.apk,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/
Build Version:2.5.0.1,18,f6e6384b899e14,subdir=RoundR,update=.;../StandOutRoundR,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/,rm=RoundR.apk
Build Version:2.6.0.0,19,b47ca72867093d,subdir=RoundR,update=.;../StandOutRoundR,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/,rm=RoundR.apk
Build Version:2.7.0.2,22,6eb2108ae73e59,subdir=RoundR,update=.;../StandOutRoundR,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/,rm=RoundR.apk
Build Version:3.0.1,24,c16d88904db105,subdir=RoundR,update=.;../StandOutRoundR,prebuild=\
rm -rf RoundR/gen/ StandOutRoundR/gen/,rm=RoundR.apk

Auto Update Mode:None
# Can cross-reference with Website, Market or apk in repo
Update Check Mode:RepoManifest
Current Version:3.0.2
Current Version Code:24

