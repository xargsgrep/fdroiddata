Categories:Reading
License:GPLv2+
Web Site:http://bytten.net
Source Code:https://github.com/tcoxon/XkcdViewer
Issue Tracker:https://github.com/tcoxon/XkcdViewer/issues

Auto Name:xkcdViewer
Summary:Viewer for the xkcd comic
Description:
Shows comics from xkcd.com, including alt-text. Also provides
support for bookmarks and easy navigation.
.

Repo Type:git
Repo:https://github.com/tcoxon/XkcdViewer.git

Build Version:2.1.2,15,20ceb28d4360b11d2bfd
Build Version:2.2.1,17,8ee6381d565db4d1b6a4
Build Version:3.0.2,21,v3.0.2,target=android-10
#Can't actually tell if it has been released
Build Version:3.9.0,30,1989cf99,srclibs=ComicViewer@18f7270c3,target=android-11,prebuild=\
sed -i 's@\(android.library.reference.1=\).*@\1$$ComicViewer$$@' project.properties
Build Version:4.0.1,31,063f36f5ee,srclibs=ComicViewer@bd1d59bc5,target=android-12,prebuild=\
sed -i 's@\(android.library.reference.1=\).*@\1$$ComicViewer$$@' project.properties
Build Version:4.1.0,32,v4.1.0,srclibs=ComicViewer@234ce1dcc,target=android-12,prebuild=\
sed -i 's@\(android.library.reference.1=\).*@\1$$ComicViewer$$@' project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:4.1.0
Current Version Code:32

