Categories:Internet
License:GPLv3+
Web Site:
Source Code:https://github.com/chris-blay/radio-reddit-for-Android
Issue Tracker:https://github.com/chris-blay/radio-reddit-for-Android/issues

Auto Name:radio reddit
Summary:Listen to music posted on Reddit
Description:
Select any subreddit from the many dedicated to music on the news site
reddit.com and listen to the songs that are posted there. Subreddits are often
separated by music genre. This simple app lets you select one of them and the
app will stream the music for you automatically.
.

Repo Type:git
Repo:https://github.com/chris-blay/radio-reddit-for-Android

Build Version:0.6,6,bc92525f6639334,subdir=radioreddit,init=rm -f ../radioreddit.apk

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.6
Current Version Code:6

