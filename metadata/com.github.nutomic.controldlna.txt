Categories:Multimedia
License:NewBSD
Web Site:https://github.com/Nutomic/control-dlna
Source Code:https://github.com/Nutomic/control-dlna
Issue Tracker:https://github.com/Nutomic/control-dlna/issues

Auto Name:ControlDLNA
Summary:DLNA/UPnP control point
Description:
ControlDLNA is a DLNA and UPnP control point app for your phone.

It lets you play audio and video from any DLNA or UPnP compatible server in
the local network to a DLNA or UPnP renderer.

Additionally, other apps can utilize the MediaRouter API to play their media
on a remote device.
.

Repo Type:git
Repo:https://github.com/Nutomic/control-dlna.git

Build:0.1.0,1
    commit=0972b7973
    srclibs=ActionBarSherlock@4.3.1
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.2.0,3
    commit=0.2.0
    srclibs=ActionBarSherlock@4.3.1
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.5.0,7
    disable=no gradle
    commit=0.5.0
    gradle=yes

Build:0.5.1,8
    commit=0.5.1
    gradle=yes

Build:0.5.2,9
    commit=0.5.2
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.5.3
Current Version Code:10

