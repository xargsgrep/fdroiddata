Categories:Wallpaper
License:GPLv3+
Source Code:https://github.com/NightlyNexus/Color-Clock
Issue Tracker:https://github.com/NightlyNexus/Color-Clock/issues

Summary:Clock widget
Description:
Simple digital clock widget in your choice of colours
.

Repo Type:git
Repo:https://github.com/NightlyNexus/Color-Clock.git

Build Version:1.0,1,64d55457728b,\
srclibs=ColorPickerPreference@15b666,\
prebuild=sed -i 's@\(reference.1=\).*@\1$$ColorPickerPreference$$@' project.properties

Auto Update Mode:None
Update Check Mode:None
Current Version:1.0
Current Version Code:1

