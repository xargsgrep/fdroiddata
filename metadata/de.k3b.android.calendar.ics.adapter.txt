Categories:Office,System
License:GPLv3
Web Site:https://github.com/k3b/CalendarIcsAdapter/wiki
Source Code:https://github.com/k3b/CalendarIcsAdapter
Issue Tracker:https://github.com/k3b/CalendarIcsAdapter/issues

Name:Calendar ICS adapter
Auto Name:android.calendar.ics.adapter
Summary:Import and export .ics calendar files
Description:
Helper that opens *.ics files or attachments of type text/calendar and 
displays the "Add to calendar" dialog pre-populated. 

It also supports exporting calendars, either
as a file or as an attachment. 

This is a fork of [[org.dgtale.icsimport]] that
adds the export functionality and tries to support Android versions prior to
4.0.

'''Required Android Permissions:''' 
* READ_CALENDAR used to export events from calendar
* WRITE_EXTERNAL_STORAGE used to store the created ics-file so that it can be attached to email or send via bluetooth.

'''Status:''' 
* Works with android 4.0 and up. Should work with most android 2.1 and up but it is not garanteed, since there was no standard calender api before android 4.0.
* Works with [[com.fsck.k9]] to import and export calender-events via ics-file-attachments.
* Works with "send to bluetooth"
* Does not work with "receive bluetooth" because android blocks receiving "*.ics" files. As a workaround you can receive "*.ics_" files via bluetooth that can be imported. 
.

Repo Type:git
Repo:https://github.com/k3b/CalendarIcsAdapter.git

Build:1.5.3-140409,9
    commit=v1.5.3

Maintainer Notes:
Jars used in 1.5.3 are the same ones that were used in org.dgtale.icsimport.
Check them again if they are changed. Will switch to source libraries or
gradle deps soon.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.5.3-140409
Current Version Code:9

