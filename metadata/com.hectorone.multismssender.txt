Categories:Phone & SMS
License:GPL
Web Site:http://mathux.dyndns.org
Source Code:http://mathux.dyndns.org/gitweb/?p=MultiSmsSender.git;a=summary
Issue Tracker:

Auto Name:Multi Sms
Summary:Send SMS to many contacts
Description:
* Select contacts with a list
* Create Group gathering several contacts together
* Integrate sent messages in the main SMS app
* Delivery reports

Coming features:
* Delivery reports support in the main SMS app
* Phone type for phone number
.

Repo Type:git
Repo:git://mathux.dyndns.org/MultiSmsSender.git

Build Version:2.0,10,5108804
Build Version:2.1,11,ae4b9c9
Build Version:2.2,12,c1cb2a3
Build Version:2.3,13,95bb2d1

Auto Update Mode:None
Update Check Mode:RepoManifest/Android2.0
Current Version:2.3
Current Version Code:13

