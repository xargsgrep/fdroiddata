Categories:Reading
License:GPLv3
Web Site:https://github.com/OnlyInAmerica/OpenSpritz-Android
Source Code:https://github.com/OnlyInAmerica/OpenSpritz-Android
Issue Tracker:https://github.com/OnlyInAmerica/OpenSpritz-Android/issues

Auto Name:OpenSpritz
Summary:ePub reader designed for fast reading
Description:
This reader is based on OpenSpritz technology and displays a stream
of word, one at each time. Words are marked and positioned for easy
and fast reading. See [https://github.com/Miserlou/OpenSpritz] for a
online demo.
.

Repo Type:git
Repo:https://github.com/OnlyInAmerica/OpenSpritz-Android.git

Build:1.0,1
    disable=unknown jars
    commit=1b30cbd993bb6fc317df40d4d8a936e89d2d9b96
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

