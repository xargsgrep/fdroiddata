Categories:Phone & SMS
License:LGPL
Web Site:http://smssync.ushahidi.com
Source Code:https://github.com/ushahidi/SMSSync
Issue Tracker:https://github.com/ushahidi/SMSSync/issues
Donate:http://ushahidi.com/get-involved

Auto Name:SMSSync
Summary:SMS to internet gateway
Description:
Have received SMS' forwarded to an internet url. Options exist to filter by keywords, to block
SMS' that don't include a secret and to identify the sender.
.

Repo Type:git
Repo:https://github.com/ushahidi/SMSSync.git

Build:1.1.9,11
    commit=42478d1ac
    target=android-4
    rm=tests/bin/smssyncTest.apk

Build:2.0.0,12
    commit=v2.0.0
    subdir=smssync
    target=android-16
    update=.;../abslib
    prebuild=rm -rf tests gen

Build:2.0.2,14
    commit=v2.0.2
    subdir=smssync
    target=android-16
    update=.;../abslib
    prebuild=rm -rf tests gen

Build:2.3,15
    commit=v2.3
    subdir=smssync
    target=android-16
    update=.;../abslib

Build:2.5.1,20
    commit=v2.5.1
    subdir=smssync
    gradle=noAnalytics
	scandelete=smssync/libs

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.5.1
Current Version Code:20

