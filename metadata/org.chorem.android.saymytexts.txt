Categories:Phone & SMS
License:GPLv3
Web Site:https://doc.chorem.org/say-my-texts
Source Code:http://forge.chorem.org/projects/say-my-texts/repository
Issue Tracker:http://forge.chorem.org/projects/say-my-texts/issues

Auto Name:Say My Texts
Summary:Read out received text messages
Description:
Reads out loud the SMS you receive while a headset is plugged
or a bluetooth handfree is connected. 
.

Repo Type:git-svn
Repo:https://svn.chorem.org/say-my-texts;trunk=trunk;tags=tags

Build:1.1,2
    disable=third party repos plus strange prepare/perform commands to be run
    commit=say-my-texts-1.1
    maven=yes

Maintainer Notes:
Fournir la version de la base à utiliser, renseigner la propriété *dbVersion*
mvn release:prepare -Darguments="-DdbVersion=2013.06.04"
mvn release:perform -Darguments="-DdbVersion=2013.06.04"

Pour préparer une release complête, lancer la commande:
mvn release:prepare -Darguments="-DperformFullRelease -DdbVersion=2013.06.04"
mvn release:perform -Darguments="-DperformFullRelease -DdbVersion=2013.06.04"
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2

