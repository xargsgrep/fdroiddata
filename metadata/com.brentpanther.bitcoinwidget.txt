Categories:Office
License:MIT
Web Site:https://github.com/hwki/SimpleBitcoinWidget
Source Code:https://github.com/hwki/SimpleBitcoinWidget
Issue Tracker:https://github.com/hwki/SimpleBitcoinWidget/issues
Bitcoin:15SHnY7HC5bTxzErHDPe7wHXj1HhtDKV7z

Auto Name:Simple Bitcoin Widget
Summary:Show current bitcoin exchange prices
Description:
A clean and simple bitcoin widget to show the current price from different
exchanges.
.

Repo Type:git
Repo:https://github.com/hwki/SimpleBitcoinWidget.git

Build:2.2,18
    commit=48863e9294902f4
    target=android-19

Build:2.3,19
    commit=2.3
    target=android-19

Build:2.4,20
    commit=2.4

Build:2.5,21
    commit=2.5

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.5
Current Version Code:21

