Categories:Development
License:GPLv3
Web Site:https://code.google.com/p/alogcat
Source Code:https://code.google.com/p/alogcat/source
Issue Tracker:https://code.google.com/p/alogcat/issues

Auto Name:aLogcat
Summary:System and app log viewer
Description:
An app equivalent of logcat from the terminal. You can filter by importance
in the settings: see only errors or view general debugging info. The result can then be filtered by keyword
so if an app has problems you can send just the relevant logs to the
developer. But watch out, the filter remains in place unless you remove it!

It might not work on Android 4.1+, since apps are not allowed to access each other's logs. In custom ROMs
with root access it should still probably work.
.

Repo Type:git-svn
Repo:https://alogcat.googlecode.com/svn/trunk

Build Version:2.1.6,34,28
Build Version:2.3,36,30
Build Version:2.3.2,38,31,prebuild=find . -type f -name \*.java -print0 | xargs -0 sed -i "s/^import org\.jtb\.alogcat\.donate\.R;$/import org.jtb.alogcat.R;/g" && sed -i "s/org.jtb.alogcat.donate/org.jtb.alogcat/" AndroidManifest.xml
Build Version:2.4,39,37
Build Version:2.5,40,41,prebuild=find . -type f -name \*.java -print0 | xargs -0 sed -i "s/^import org\.jtb\.alogcat\.donate\.R;$/import org.jtb.alogcat.R;/g" && sed -i "s/org.jtb.alogcat.donate/org.jtb.alogcat/" AndroidManifest.xml
Build Version:2.6.1,43,48,prebuild=find . -type f -name \*.java -print0 | xargs -0 sed -i "s/^import org\.jtb\.alogcat\.donate\.R;$/import org.jtb.alogcat.R;/g" && sed -i "s/org.jtb.alogcat.donate/org.jtb.alogcat/" AndroidManifest.xml

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.6.1
Current Version Code:43

